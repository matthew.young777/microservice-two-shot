import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatsList from './HatsList';
import React from 'react';
import ShoeForm from "./NewShoe";
import ShoePage from "./ShoeList";



function App() {
  // if (props.hats === undefined) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatsList />} />
          </Route>
          <Route path="shoes" element={<ShoePage />} />
          <Route path="shoes/new" element={<ShoeForm />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
