import React from "react";

// function HatsList(props) {
//     return (
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Fabric</th>
//             <th>Color</th>
//             <th>Location</th>
//           </tr>
//         </thead>
//         <tbody>
//           {props.hats.map(hat => {
//             return (
//               <tr key={hat.href}>
//                 <td>{ hat.name }</td>
//                 <td>{ hat.fabric }</td>
//                 <td>{ hat.color }</td>
//                 <td>{ hat.location }</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     );
//   }
  
//   export default HatsList;

class HatsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            name: '',
            color: '',
            pictureurl: '',
            locations: '',
            hats:[]
          };
          this.handleDelete = this.handleDelete.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ hats: data.hats });
        }
}

async handleDelete(e) {
    console.log("test")
    console.log(e)
    const HatUrl = `http://localhost:8090/api/hats/${e.target.value}`;
    const fetchConfig = {
      method: "delete",
    };
    let response = await fetch(HatUrl, fetchConfig);
    if (response.ok) {
        let deleteHat = await response.json();
        console.log(deleteHat);
        window.location.reload()
      }
};

render() {
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
            <th>Image</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {this.state.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td><img src={hat.picture_url} className="w-25 p-3" /></td>
                <td><button onClick={this.handleDelete} value={hat.id} className="btn btn-primary">Delete</button></td>

              </tr>
            );
          })}
        </tbody>
      </table>
    );
}
}
export default HatsList;


//   async function loadAttendees() {
//     const response = await fetch('http://localhost:8001/api/attendees/');
//     if (response.ok) {
//       const data = await response.json();
//       root.render(
//         <React.StrictMode>
//           <App attendees={data.attendees} />
//         </React.StrictMode>
//       );
//     } else {
//       console.error(response);
//     }
//   }
//   loadAttendees();