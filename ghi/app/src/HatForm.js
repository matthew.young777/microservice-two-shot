import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            name: '',
            color: '',
            pictureurl: '',
            locations: []
          };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureURLChange = this.handlePictureURLChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }
      handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
      }
      handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
      }
      handlePictureURLChange(event) {
        const value = event.target.value;
        this.setState({pictureurl: value})
      }
      handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
      }
      async handleSubmit(event) {
        event.preventDefault();
        // console.log(this.state)
        const data = {...this.state};
        data.picture_url = data.pictureurl;
        delete data.pictureurl;
        delete data.locations;
        console.log(data);
      
        const HatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(HatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
        
            const cleared = {
                fabric: '',
                name: '',
                color: '',
                pictureurl: '',
                location: '',
            };
            this.setState(cleared);
          }
      }
      async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ locations: data.locations });
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //     const option = document.createElement('option');
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureURLChange} value={this.state.pictureurl} placeholder="pictureurl" required type="text" name="pictureurl" id="pictureurl" className="form-control" />
                                <label htmlFor="pictureurl">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default HatForm;
