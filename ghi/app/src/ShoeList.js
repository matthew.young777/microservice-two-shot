import React from "react";
import { Link } from "react-router-dom";

function ShoeColumn(props) {
    // console.log("props.list");
    // console.log(props.list);

    const handleDelete = async (id) => {
        const shoeURL = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: "delete",
        };
        let response = await fetch(shoeURL, fetchConfig);
        if (response.ok) {
            response = await response.json();
            console.log(response);
            window.location.reload();
        }
    };

    return (
        <div className="col">
            {props.list.map((shoe) => {
                return (
                    <div key={shoe.href} className="card mb-3 shadow">
                        <img src={shoe.picture_url} className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{shoe.name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {shoe.manufacturer}
                            </h6>
                            <p className="card-text">{shoe.color}</p>

                            <button
                                onClick={() => handleDelete(shoe.id)}
                                className="btn btn-primary"
                            >
                                Delete
                            </button>
                        </div>
                        {/* <div className="card-footer"></div> */}
                    </div>
                );
            })}
        </div>
    );
}

class ShoePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoeColumns: [[], [], []],
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/shoes/";
        const shoeColumns = [[], [], []];
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                let i = 0;
                for (const shoe of data.shoes) {
                    shoeColumns[i].push(shoe);
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                }
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error(e);
        }
        this.setState({ shoeColumns: shoeColumns });
        // console.log(shoeColumns);
    }
    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                    {/* <img
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/logo.svg"
                        alt=""
                        width="600"
                    /> */}
                    <h1 className="display-5 fw-bold">Shoes!</h1>
                    {/* <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">Here are your shoes</p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <Link
                                to="/attendees/new"
                                className="btn btn-primary btn-lg px-4 gap-3"
                            >
                                Attend a conference
                            </Link>
                        </div>
                    </div> */}
                </div>
                <div className="container">
                    <h2>Shoes</h2>
                    <div className="row">
                        {this.state.shoeColumns.map((shoeList, index) => {
                            return <ShoeColumn key={index} list={shoeList} />;
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default ShoePage;
