from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.PROTECT, blank=True)
    picture_url = models.URLField(null=True)

    # def get_api_url(self):
    #     return reverse("api_show_shoe", kwargs={"pk": self.pk})
    


