import json
import requests
import os

KEY = os.environ.get("PEXELS_API_KEY")
def get_photo(name, manufacturer, color):
    headers = {"Authorization": KEY}
    params = {
        "per_page": 1,
        "query": f"{color} {manufacturer} {name}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}