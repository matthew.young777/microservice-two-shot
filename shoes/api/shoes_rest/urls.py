from django.urls import path

from .views import  api_list_shoes, api_shoe_detail

urlpatterns = [
    path("shoes/", api_list_shoes, name="list_shoes"),
    # path("shoes/<int:bin_vo_id", api_list_shoes, name="create_shoe"),
    path("shoes/<int:pk>/", api_shoe_detail, name="single_shoe"),
]