from django.db import models
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=300, null=True)



class Hat(models.Model):

    fabric = models.CharField(max_length=200)
    name = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT, blank=True)

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})